from typing import List


class Facet:
    normal: List[float]

    def __init__(self, n: List[float]):
        self.normal = n


class FacetData:
    illumination: float
    sample: float

    def __init__(self, illumination: float, sample: float):
        self.illumination = illumination
        self.sample = sample
