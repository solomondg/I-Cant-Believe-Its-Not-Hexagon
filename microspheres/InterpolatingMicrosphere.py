from typing import List

import numpy as np
import scipy as sp

from microspheres.Facet import *
from microspheres.UnitSphereRandomVectorGenerator import UnitSphereRandomVectorGenerator


class InterpolatingMicrosphere:
    microsphere: List[Facet]
    microsphereData: List[FacetData]
    dimension: int  # Space dimension
    size: int  # num of surface elements/facets
    maxDarkFraction: float  # max fraction of facets that can be dark
    darkThreshold: float  # lowest nonzero illumination
    background: float  # background value

    def __init__(self, dimension: int, size: int, maxDarkFraction: float, darkThreshold: float, background: float,
                 rand: UnitSphereRandomVectorGenerator = None):
        assert dimension > 0
        assert size > 0
        assert darkThreshold >= 0
        assert 0 < maxDarkFraction < 1

        self.dimension, self.size, self.maxDarkFraction, self.darkThreshold, self.background = \
            dimension, size, maxDarkFraction, darkThreshold, background

        self.microsphere: List[Facet] = [None] * size
        self.microsphereData: List[FacetData] = [None] * size

        if rand is not None:
            for i in range(0, self.size):
                self.add(rand.nextVector())

    def add(self, normal: List[float]):
        assert len(self.microsphere) <= self.size
        assert len(normal) <= self.dimension
        self.microsphere.append(Facet(n=normal))
        self.microsphereData.append(FacetData(illumination=0, sample=0))

    def value(self, point: List[float], samplePoints: List[List[float]], sampleValues: List[float],
              exponent: float, noInterpolationTolerance: float) -> float:
        assert exponent >= 0
        assert len(samplePoints) == len(sampleValues)
        #assert len(samplePoints[0]) == len(samplePoints)
        self.clear()

        point: np.ndarray = np.asarray(point)
        samplePoints: np.ndarray = np.asarray(samplePoints)
        sampleValues: np.ndarray = np.asarray(sampleValues)

        numSamples = len(samplePoints)
        for i in range(0, numSamples):
            diff: np.ndarray = samplePoints[i] - point
            diffNorm: float = np.linalg.norm(diff)
            if abs(diffNorm) < noInterpolationTolerance:
                return sampleValues[i]
            weight: float = diffNorm ** (-exponent)
            self.illuminate(list(diff), sampleValues[i], weight)

        return self.interpolate()

    def interpolate(self) -> float:
        darkCount: int = 0
        value: float = 0
        totalWeight: float = 0
        for fd in self.microsphereData:
            iV: float = fd.illumination
            if iV != 0:
                value += iV * fd.sample
                totalWeight += iV
            else:
                darkCount += 1
        darkFraction: float = darkCount / self.size
        if darkFraction >= self.maxDarkFraction:
            return value / totalWeight
        else:
            return self.background

    def illuminate(self, sampleDirection: List[float], sampleValue: float, weight: float):
        sampleDirection: np.ndarray = np.asarray(sampleDirection)
        for i in range(0, self.size):
            n: np.ndarray = np.asarray(self.microsphere[i].normal)
            cos: float = np.dot(n, sampleDirection) / (np.linalg.norm(n) * np.linalg.norm(sampleDirection))
            if cos > 0:
                illumination: float = cos * weight
                if illumination > self.darkThreshold and illumination > self.microsphereData[i].illumination:
                    self.microsphereData[i] = FacetData(illumination=illumination, sample=sampleValue)

    def clear(self):
        for i in range(0, self.size):
            self.microsphereData[i] = FacetData(illumination=0, sample=0)
