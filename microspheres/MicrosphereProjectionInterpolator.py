from typing import List

from microspheres.InterpolatingMicrosphere import InterpolatingMicrosphere

from microspheres.InterpolatingMicrosphere2D import InterpolatingMicrosphere2D


class MicrosphereProjectionInterpolator:
    exponent: float  # brightness exponent
    microsphere: InterpolatingMicrosphere  # microsphere
    sharedSphere: bool  # whether or not to share the sphere
    noInterpolationTolerance: float  # tolerance value below which no interpolation is necessary

    def __init__(self, *,
                 exponent: float,
                 sharedSphere: bool,
                 noInterpolationTolernace: float,
                 dimension: int = None,
                 elements: int = None,
                 maxDarkFraction: float = None,
                 darkThreshold: float = None,
                 background: float = None,
                 microsphere: InterpolatingMicrosphere = None):
        assert (microsphere is not None) or \
               (dimension is not None and
                elements is not None and
                maxDarkFraction is not None and
                darkThreshold is not None and
                background is not None)

        if microsphere is None:
            microsphere = InterpolatingMicrosphere(dimension=dimension, size=elements,
                                                   maxDarkFraction=maxDarkFraction,
                                                   darkThreshold=darkThreshold, background=background)

        assert exponent >= 0
        self.microsphere = microsphere
        self.exponent = exponent
        self.sharedSphere = sharedSphere
        self.noInterpolationTolerance = noInterpolationTolernace

    def interpolate(self, xval: List[List[float]], yval: List[float]):
        assert (xval is not None) and (yval is not None)
        assert len(xval) != 0
        assert len(xval) == len(yval)
        assert xval[0] != None
        dimension: int = self.microsphere.dimension
        assert dimension == len(xval[0])
        #assert len(xval[0]) == len(xval)

        m = self.microsphere
        return lambda point: m.value(point, xval, yval, self.exponent, self.noInterpolationTolerance)
