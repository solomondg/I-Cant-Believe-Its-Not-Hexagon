from math import sqrt
import random
from typing import List


class UnitSphereRandomVectorGenerator:
    dimension: int

    def __init__(self, dimension: int):
        self.dimension = dimension

    def nextVector(self) -> List[float]:
        normSq: float = 0
        v: List[float] = [0] * self.dimension
        for i in range(0, self.dimension):
            comp = random.gauss(0, 1)
            v[i] = comp
            normSq += comp * comp

        f = 1 / sqrt(normSq)
        v = [i * f for i in v]

        return v
