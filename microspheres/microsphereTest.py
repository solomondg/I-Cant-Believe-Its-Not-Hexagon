from microspheres.InterpolatingMicrosphere2D import InterpolatingMicrosphere2D
from microspheres.MicrosphereProjectionInterpolator import MicrosphereProjectionInterpolator

ms = MicrosphereProjectionInterpolator(exponent=2, sharedSphere=True,
                                       noInterpolationTolernace=0.01,
                                       microsphere=InterpolatingMicrosphere2D(size=2500, maxDarkFraction=0.01,
                                                                              darkThreshold=0.01, background=0))

xval = [
    [0, 0],
    [5, 0],
    [0, 5],
    [5, 5]
]

yval = [
    10,
    5,
    5,
    0
]

proj = ms.interpolate(xval=xval, yval=yval)
csv = ""
i, j = 0, 0
while i < 5:
    while j < 5:
        res = proj([i, j])
        csv += str(i) + "," + str(j) + "," + str(res) + "\n"
        j += 0.25
        print(i, j)
    i += 0.25
    j = 0

with open('data.csv', 'w') as f:
    f.write(csv)
