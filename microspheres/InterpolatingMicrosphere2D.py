from math import pi, sin, cos
from typing import List

from microspheres.Facet import Facet
from microspheres.InterpolatingMicrosphere import InterpolatingMicrosphere


class InterpolatingMicrosphere2D(InterpolatingMicrosphere):
    DIMENSION: int = 2

    def __init__(self, size: int, maxDarkFraction: float, darkThreshold: float, background: float):
        super(InterpolatingMicrosphere2D, self).__init__(dimension=self.DIMENSION, size=size,
                                                         maxDarkFraction=maxDarkFraction, darkThreshold=darkThreshold,
                                                         background=background)
        self.microsphere: List[Facet] = []
        for i in range(0, size):
            angle: float = i * pi * 2 / size
            self.add([cos(angle), sin(angle)])
