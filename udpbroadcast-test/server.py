import socket
import time

sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

BROADCAST_IP = '<broadcast>'  # Try 255.255.255.255 if this doesn't work
BROADCAST_PORT = 42069  # nice

sock.bind((BROADCAST_IP, BROADCAST_PORT))

for i in range(0, 1000):
    sock.sendto(bytes(("A"+ str(i))*256, 'utf-8'), (BROADCAST_IP, BROADCAST_PORT))
    #time.sleep(0.000001)
