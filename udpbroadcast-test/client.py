import socket
import cv2

sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

#sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

BROADCAST_PORT = 42069  # nice
sock.bind(('', BROADCAST_PORT))

while True:
    ret = sock.recvfrom(512)
    print(ret)
    print(type(ret[0]))
