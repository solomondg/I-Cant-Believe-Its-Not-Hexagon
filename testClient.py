#!/usr/bin/env python3
import cv2
import socket
from threading import Thread
from typing import List, Dict
from matplotlib import pyplot as plt
import time
import numpy as np

from microspheres.InterpolatingMicrosphere2D import InterpolatingMicrosphere2D
from microspheres.MicrosphereProjectionInterpolator import MicrosphereProjectionInterpolator

sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
BROADCAST_PORT = 42069  # nice
sock.bind(('', BROADCAST_PORT))


def gb():
    global parseQueue
    global x
    global y
    global receivedStatusFrame
    global image
    global placedPixels
    global interpImage


gb.parseQueue: List[bytes] = []
gb.receivedStatusFrame: bool = False
gb.image: np.ndarray = None
gb.placedPixels: np.ndarray = None
gb.interpImage: np.ndarray = None


def listenLoop():
    while True:
        ret = sock.recvfrom(512)  # buffer size
        msg = ret[0]
        # print("received message", [bytes([x]) for x in msg])
        gb.parseQueue.append(msg)


def parseLoop():
    while True:
        if len(gb.parseQueue) == 0:
            continue
        packet = gb.parseQueue.pop(0)
        # print("parsing", [bytes([x]) for x in packet], [x for x in packet])
        firstByte = packet[0]
        if bytes([firstByte]) == bytes([0]):
            # status frame
            # print("status frame")
            parseStatusFrame(packet)
        elif bytes([firstByte]) == bytes([1]) or bytes([firstByte]) == bytes(
                [0b0001 << 4]):  # cause of the padding thing
            # data frame
            # print("data frame")
            parseDataFrame(packet)


def parseStatusFrame(packet: bytes):
    lowerX = packet[1]
    lowerY = packet[2]
    upperXupperY = packet[3]
    upperX = (upperXupperY >> 4) << 8
    upperY = (upperXupperY & 0b1111) << 8
    x = upperX | lowerX
    y = upperY | lowerY
    print("Received status frame with x=", x, "y=", y)
    if gb.image is None:
        gb.image = np.zeros(shape=(y, x, 3))
        gb.placedPixels = np.zeros(shape=(y, x))
    gb.receivedStatusFrame = True


def parseDataFrame(packet: bytes):
    if gb.receivedStatusFrame:
        # gotta figure out how to decode status frame aaaaaaaa
        # first byte frame type
        packet = packet[1:]
        # now e gget rid of unnecedary

        # [
        #     package type (uint4)
        #     4 bits of 0 padding lmao
        #     Pixel chunk xy (24 bit)
        # Pixel data (32 * 16 = 512 bit) in simple left-right then top-bottom order
        # ]
        # [
        #     r (uint5)
        #     g (uint6)
        #     b (uint5)
        # ]
        #

        lowerX = packet[0]
        lowerY = packet[1]
        upperXupperY = packet[2]
        upperX = (upperXupperY >> 4) << 8
        upperY = (upperXupperY & 0b1111) << 8
        xCoord = upperX | lowerX
        yCoord = upperY | lowerY
        packet = packet[3:]
        # print(len(packet))
        assert len(packet) == 64
        pixels: Dict[(int, int):(int, int, int)] = {}
        reconstructedPackets = []
        for i in range(0, len(packet), 2):
            lower = packet[i]
            upper = packet[i + 1]
            pixelData = (upper << 8) | lower
            reconstructedPackets.append(pixelData)

        assert len(reconstructedPackets) == 32
        for i in range(len(reconstructedPackets)):
            # [r, r, r, r, r, g, g, g, g, g, g, b, b, b, b, b]
            pixelData = reconstructedPackets[i]
            blue = pixelData & 0b11111
            green = (pixelData >> 5) & 0b111111
            red = (pixelData >> 11) & 0b11111
            rem = i // 8
            y = rem
            x = i - 8 * rem
            pixels[(xCoord + x * gb.image.shape[1] // 8, yCoord + y * gb.image.shape[0] // 4)] = (
                int(blue << 3), int(green << 2), int(red << 3))

        for i in list(pixels.keys()):
            # print("pixel at y=", i[1], 'x=', i[0], 'with rgb of', np.asarray(pixels[i]))
            gb.image[i[1]][i[0]] = np.asarray([pixels[i][0], pixels[i][1], pixels[i][2]], dtype=np.uint8)
            gb.placedPixels[i[1]][i[0]] = 1


def displayLoop():
    cv2.namedWindow("image", cv2.WINDOW_NORMAL)
    i = 0
    while True:
        if gb.receivedStatusFrame is True:
            i += 1
            #interp()
            # plt.imshow(gb.image)
            # plt.xticks([])
            # plt.yticks([])
            # plt.show()
            # cv2.imshow("image", gb.image)
            # cv2.waitKey(1)
            print("saving to {0}".format(str(i)))
            cv2.imwrite("output/cat{0}.png".format(str(i)), gb.image)
            time.sleep(1 / 60)


slidingWindowX = 10
slidingWindowY = 10

ms = MicrosphereProjectionInterpolator(exponent=2, sharedSphere=True,
                                       noInterpolationTolernace=0.01,
                                       microsphere=InterpolatingMicrosphere2D(size=10, maxDarkFraction=0.01,
                                                                              darkThreshold=0.01, background=0))


def interp():
    imgRed: np.ndarray = gb.image[:, :, 2]  # 2 1 0 cause BGR
    imgGreen: np.ndarray = gb.image[:, :, 1]
    imgBlue: np.ndarray = gb.image[:, :, 0]

    unplacedPixels: List[int] = []
    placedPixels: List[int] = []
    i: int
    j: int
    for i in range(0, gb.placedPixels.shape[1]):
        for j in range(0, gb.placedPixels.shape[0]):
            if gb.placedPixels[j][i] == 1:
                placedPixels.append([i, j])
            else:
                unplacedPixels.append([i, j])
    k: List[int, int]

    gb.interpImage = gb.image.copy()  # We'll be placing interpolated pixels directly into this
    image = gb.image.copy()

    n = 0
    o = len(unplacedPixels)
    for k in unplacedPixels:
        #containedXRange = range(k[0] - slidingWindowX // 2, k[0] + slidingWindowX // 2)
        #containedYRange = range(k[1] - slidingWindowY // 2, k[1] + slidingWindowY // 2)
        #l: List[int, int]
        #influencePoints = [l for l in placedPixels if (l[0] in containedXRange and l[1] in containedYRange)]

        #influenceSlice = image \
        #                     [k[1] - slidingWindowY // 2:k[1] + slidingWindowY // 2] \
        #    [k[0] - slidingWindowX // 2:k[0] + slidingWindowX // 2]

        influencePoints = []
        for i in range(k[1]-slidingWindowY//2,k[1]+slidingWindowY//2):
            for j in range(k[0]-slidingWindowX//2,k[0]+slidingWindowX//2):
                influencePoints.append([i,j])
        xVal = influencePoints
        print("point: ", k, " influencing points: ", influencePoints)
        #if len(xVal) == 0:
        #    return
        n += 1

        redVals = [imgRed[m[0]][m[1]] for m in influencePoints]
        greenVals = [imgGreen[m[0]][m[1]] for m in influencePoints]
        blueVals = [imgBlue[m[0]][m[1]] for m in influencePoints]

        redInterp = ms.interpolate(xval=xVal, yval=redVals)([k[0], k[1]])
        greenInterp = ms.interpolate(xval=xVal, yval=greenVals)([k[0], k[1]])
        blueInterp = ms.interpolate(xval=xVal, yval=blueVals)([k[0], k[1]])
        gb.interpImage[k[1]][k[0]] = np.asarray(blueInterp, greenInterp, redInterp)


Thread(target=listenLoop, args=(), daemon=False).start()
Thread(target=parseLoop, args=(), daemon=False).start()
Thread(target=displayLoop, args=(), daemon=False).start()

input("press enter to quit\n")
