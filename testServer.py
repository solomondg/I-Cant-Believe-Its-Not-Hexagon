#!/usr/bin/env python3
import sys

import cv2
import socket
import time
from math import sqrt
from random import randint
from threading import Thread

import numpy as np
import bitstring
import bitarray


class LmaoBruhYouFuckedUpException(Exception):
    pass


# idiot python
sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

BROADCAST_IP = '<broadcast>'  # Try 255.255.255.255 if this doesn't work
#BROADCAST_IP = '127.0.0.1'  # Try 255.255.255.255 if this doesn't work
BROADCAST_PORT = 42069  # nice

sock.bind((BROADCAST_IP, BROADCAST_PORT))

imageX = 1000

maxQueueBuffer = 100

assert imageX % 2 == 0  # Assert even
assert (imageX // 8) * 8 == imageX  # Assert int


def gb():
    global packetQueue  # We'll add packets to send here and then send them in another "thread" <- thx python
    global sendPixelOrder  # Order to send the pixel to maximize distance between each sent pixel
    global sourceImage


gb.packetQueue: [bytes] = []
gb.sendPixelOrder: {int: (int, int)} = {}
gb.sourceImage = np.ndarray(shape=(imageX // 2, imageX, 3))


def calcPlaceOrder():
    xMax, yMax = gb.sourceImage.shape[:2][::-1]
    xMax //= 8
    yMax //= 4
    placedPixels: [(int, int)] = []
    availPixels: [(int, int)] = []

    pixelNumber = 0

    for i in range(xMax):
        for j in range(yMax):
            availPixels.append((i, j))

    while len(availPixels) > 0:
        placedPixels.append(availPixels.pop(randint(0, len(availPixels) - 1)))

    for i in range(len(placedPixels)):
        gb.sendPixelOrder[i] = placedPixels[i]

    # # place routine
    # pixelNumber += 1
    # gb.sendPixelOrder[pixelNumber] = (0, 0)
    # placedPixels.append((0, 0))

    # # If I could use APL here my life would be good
    # while pixelNumber < ((xMax) * (yMax)):
    #     candidates: [(int, int)] = []
    #     for i in range(xMax):
    #         for j in range(yMax):
    #             if (i, j) not in placedPixels:
    #                 candidates.append((i, j))

    #     rankings: [(int, int), float] = []
    #     for i in candidates:
    #         minDistance = 9999999
    #         for j in placedPixels:
    #             distance = sqrt((i[0] - j[0]) ** 2 + (i[1] - j[1]) ** 2)
    #             if distance < minDistance:
    #                 minDistance = distance
    #         rankings.append([i, minDistance])
    #     x, y = sorted(rankings, key=lambda k: k[1], reverse=True)[0][0]
    #    pixelNumber += 1
    #    gb.sendPixelOrder[pixelNumber] = (x, y)
    #    placedPixels.append((x, y))


def broadcastStatusFrame():
    image = gb.sourceImage
    # packet looks like
    # [
    # uint8: 0               <- frame type
    # uint12: sourceImage.x  <- source image x
    # uint12: sourceImage.y  <- source image y
    # ]

    # now the question is how the fuck we do byte level shit in python
    # Transmit as three bytes?
    # let's just cheat for now

    x, y = gb.sourceImage.shape[:2][::-1]

    lowerBitmask = 0b000011111111
    upperBitmask = 0b111100000000

    packet = b''
    packet += bytes([0])  # def frame
    lowerX = x & lowerBitmask
    lowerY = y & lowerBitmask

    upperX = (x & upperBitmask) >> 8
    upperY = (y & upperBitmask) >> 8
    # lower x (8bit) then lower y (8bit) then upper x(4bit) then upper y (4bit)
    packet += bytes([lowerX])
    packet += bytes([lowerY])
    packet += bytes([upperX << 4 | upperY])
    print([bytes([x]) for x in packet])
    gb.packetQueue.append(packet)


def broadcastPixelChunk(xCoord: int, yCoord: int):
    xMax, yMax = gb.sourceImage.shape[:2][::-1]
    xMax //= 8
    yMax //= 4
    xVals = [xCoord + xMax * i for i in range(0, 8)]
    yVals = [yCoord + yMax * i for i in range(0, 4)]
    points = []

    for j in yVals:
        for i in xVals:
            points.append([i, j])

    pixels: [(float, float, float)] = []
    x, y = 0, 0
    for i in points:
        x, y = i
        b, g, r = gb.sourceImage[y][x]
        pixels.append((r, g, b))

    assert len(pixels) == 32

    # [
    #     package type (uint4)
    #     4 bits of 0 padding lmao
    #     Pixel chunk xy (24 bit)
    # Pixel data (32 * 16 = 512 bit) in simple left-right then top-bottom order
    # ]
    # [
    #     r (uint5)
    #     g (uint6)
    #     b (uint5)
    # ]
    #

    # We have pixel chunk x,y (0,0 is upper left hand corner)
    # Now we need to convert x,y to 12bit numbers
    try:  # let's just make sure that x and y can be represented in 12 bits (we'll need this to be the case so we can pack it all later
        assert x & 0b111111111111 == x and y & 0b111111111111 == y
    except:
        raise LmaoBruhYouFuckedUpException()

    packets = []

    for p in pixels:
        r, g, b = p

        # make sure all this shit is 8 bit max
        assert r & 0b11111111 == r and g & 0b11111111 == g and b & 0b11111111 == b

        r5bit = (r & 0b11111000) >> 3
        g6bit = (g & 0b11111100) >> 2
        b5bit = (b & 0b11111000) >> 3
        assert r5bit & 0b11111 == r5bit and g6bit & 0b111111 == g6bit and b5bit & 0b11111 == b5bit
        # [a b c d e] [f g h i j k] [l m n o p] to
        # [a b c d e f g h] [i j k l m n o p]
        # oof

        # 0000000000011111
        # to
        # 1111100000000000
        firstChunk = r5bit << 11
        # 1111100000222222
        # to
        # 1111122222200000
        secondChunk = g6bit << 5
        lastChunk = b5bit
        packet16bit = firstChunk | secondChunk | lastChunk
        packets.append(packet16bit)

    assert len(packets) == 32

    giantAss512BitNumber = 0

    splitPackets = []  # packets split from [16bit, 16bit] to [byte, byte, byte, byte]

    for i in range(len(packets)):
        # Each pixel is 16 bits
        # we need to fill 512 bits
        # can we just repeatedly shift left 16 bits?

        first = packets[i] & 0b11111111
        second = (packets[i] >> 8) & 0b11111111
        splitPackets.append(first)
        splitPackets.append(second)

        packets[i] = packets[i] << (31 - i) * 16

    for i in packets:
        giantAss512BitNumber &= i

    # start packet construction
    packet = b''
    frameTypeAndPadding = bytes([0b00010000])  # exact same as just bytes[0] but it's nice lol
    packet += frameTypeAndPadding

    # So now we have our aptly names giant ass 512 bit number
    # now to move our x,y 12 bit numbers into a 24 bit one

    lowerBitmask = 0b000011111111
    upperBitmask = 0b111100000000

    lowerX = xCoord & lowerBitmask
    lowerY = yCoord & lowerBitmask

    upperX = (xCoord & upperBitmask) >> 8
    upperY = (yCoord & upperBitmask) >> 8
    # lower x (8bit) then lower y (8bit) then upper x(4bit) then upper y (4bit)
    packet += bytes([lowerX])
    packet += bytes([lowerY])
    packet += bytes([upperX << 4 | upperY])
    packet += bytes(splitPackets)
    gb.packetQueue.append(packet)


def queuePixels():
    j = len(list(gb.sendPixelOrder.keys()))
    for i in range(j):
        print(i, 'of', j)
        # print("broadcasting chunk at", coord[0] * 8, coord[1] * 4)
        print("queue is ", len(gb.packetQueue))
        while len(gb.packetQueue) > maxQueueBuffer:
            time.sleep(1 / 10000)
        coord = gb.sendPixelOrder[i]
        broadcastPixelChunk(coord[0], coord[1])


def broadcastFromQueue():
    if len(gb.packetQueue) == 0:
        return
    n = gb.packetQueue.pop(0)
    # print("Sending packet with data", [bytes([x]) for x in n])
    sock.sendto(n, (BROADCAST_IP, BROADCAST_PORT))


def broadcastLoop():
    while True:
        broadcastFromQueue()
        time.sleep(1. / 1000)


Thread(target=broadcastLoop, daemon=True, name="rart", args=()).start()

calcPlaceOrder()
broadcastStatusFrame()
img = cv2.imread('garfielf.png')
assert img.shape == gb.sourceImage.shape
gb.sourceImage = img.copy()

# broadcastPixelChunk(0, 0)
# sys.exit(0)

queuePixels()
while len(gb.packetQueue) > 0:
    broadcastFromQueue()
