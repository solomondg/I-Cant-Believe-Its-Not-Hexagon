import cv2
import numpy as np
import scipy as sp
from scipy.interpolate import interp2d

from microspheres.InterpolatingMicrosphere2D import InterpolatingMicrosphere2D
from microspheres.MicrosphereProjectionInterpolator import MicrosphereProjectionInterpolator

image = cv2.imread("output/cat10.png")[::10, ::10]

redChannel = image[:, :, 2]
greenChannel = image[:, :, 1]
blueChannel = image[:, :, 0]

x = []
ry = []
by = []
gy = []

for j in range(image.shape[0]):
    for i in range(image.shape[1]):
        x.append([i, j])
        ry.append(redChannel[j][i])
        by.append(blueChannel[j][i])
        gy.append(greenChannel[j][i])

ms = MicrosphereProjectionInterpolator(exponent=2, sharedSphere=True,
                                       noInterpolationTolernace=3,
                                       microsphere=InterpolatingMicrosphere2D(size=10, maxDarkFraction=0.01,
                                                                              darkThreshold=0.01, background=0))

print("f")
l = len(x) * 3
j = 0

y = [m[1] for m in x]
x = [m[0] for m in x]
val = interp2d (x,y,ry)
for i in x:
    j += 1
    redChannel[x[1]][x[0]] = val([i[0], i[1]])
    print(redChannel[x[1]][x[0]])
    print(j, "of", l)
val = interp2d (x,y,ry)
for i in x:
    j += 1
    greenChannel[x[1]][x[0]] = val([i[0], i[1]])
    print(greenChannel[x[1]][x[0]])
    print(j, "of", l)
val = interp2d (x,y,ry)
for i in x:
    j += 1
    blueChannel[x[1]][x[0]] = val([i[0], i[1]])
    print(blueChannel[x[1]][x[0]])
    print(j, "of", l)

# rgb = np.stack(blueChannel, greenChannel, redChannel)
for i in range(image.shape[0]):
    for j in range(image.shape[1]):
        image[i][j] = np.asarray(blueChannel[i][j], greenChannel[i][j], redChannel[i][j])

try:
    cv2.imwrite("e.png", image)
except:
    pass
try:
    cv2.imwrite(image, "e.png")
except:
    pass
